package eo.easimer.advpdxdemo;

import eo.easimer.advpdxdemo.entities.BaseEntity;
import java.util.HashMap;
import eo.easimer.advpdxdemo.entities.Entity;
import eo.easimer.advpdxdemo.entities.Player;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsfml.graphics.FloatRect;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class EntitySystem {

    private HashMap<Integer, Entity> entities;

    private static volatile EntitySystem instance;

    public static int playerID = -1;

    public static synchronized EntitySystem getInstance() {
        if (instance == null) {
            instance = new EntitySystem();
        }
        return instance;
    }

    public static synchronized EntitySystem newInstance() {
        instance = new EntitySystem();
        return instance;
    }

    public Entity getEntity(Integer id) {
        return entities.containsKey(id) ? entities.get(id) : null;
    }

    private EntitySystem() {
        entities = new HashMap<>();
    }

    public Player getPlayer() {
        return (Player) getEntity(playerID);
    }

    public int addEntity(Entity ent) {
        int id = -1;
        if (!entities.containsValue(ent)) {
            id = entities.keySet().toArray().length;
            entities.put(id, ent);
            if (ent.getClass().toString().endsWith("Player")) {
                playerID = id;
                System.out.format("Player ID: %d\n", playerID);
            }
            System.out.format("Added entity with class \"%s\" with ID %d.\n", ent.toString(), id);
            try {
                ent.init();
            } catch (IOException ex) {
                Logger.getLogger(EntitySystem.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return id;
    }

    public void removeEntity(int index) {

        if (entities.containsKey(index)) {
            entities.remove(index);
        }
    }

    public void removeEntity(Entity ent) {
        if (entities.containsValue(ent)) {
            entities.values().remove(ent);
        }
    }

    public Boolean tick(double dt) {
        try {
            for (Entity e : entities.values()) {
                e.tick(dt);
            }
        } catch (ConcurrentModificationException ex) { //well, if some motherfucker is modifying the hashmap, then we're not iterating thru it. god fucking damnit java you fucking piece of shit
            return false;
        }
        return true;
    }

    public Boolean draw(RenderTarget rt) {
        try {
            for (Entity e : entities.values()) {
                e.draw(rt);
            }
        } catch (ConcurrentModificationException ex) {
            return false;
        }
        return true;
    }

    public Boolean event(Event event, Event.Type eventType) {
        try {
            for (Entity e : entities.values()) {
                e.event(event, eventType);
            }
        } catch (ConcurrentModificationException ex) {
            return false;
        }
        return true;
    }

    public BaseEntity[] RectCollision(FloatRect rect) {
        BaseEntity[] collidedWith = new BaseEntity[entities.values().size()];
        int i = 0;
        for (Entity e : entities.values()) {
            try {
                BaseEntity ent = (BaseEntity) e;
                if (ent.sprite.getGlobalBounds().intersection(rect) != null) {
                    collidedWith[i++] = ent;
                }
            } catch (ClassCastException ex) {

            }
        }
        return collidedWith;
    }
}
