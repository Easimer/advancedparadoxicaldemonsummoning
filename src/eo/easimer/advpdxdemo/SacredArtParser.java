package eo.easimer.advpdxdemo;

import eo.easimer.advpdxdemo.Game.Element;
import static eo.easimer.advpdxdemo.Game.Element.*;
import eo.easimer.advpdxdemo.entities.BaseEntity;
import eo.easimer.advpdxdemo.entities.EnemyMinion;
import eo.easimer.advpdxdemo.entities.Entity;
import eo.easimer.advpdxdemo.entities.FriendlyFriend;
import java.io.IOException;
import java.io.InputStream;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.jsfml.graphics.Color;
import org.jsfml.system.Vector2i;

/**
 *
 * @author easimer
 */
public class SacredArtParser {

    public static class Tuple<X, Y> {

        public final X x;
        public final Y y;

        public Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }

    public static HashMap<Integer, Tuple<String, Color>> log = new HashMap<>();

    public static void putLog(String logmsg, BaseEntity caster) {
        log.put(log.size(), new Tuple<>(logmsg, (caster.equals(Game.getInstance().player)) ? new Color(255, 255, 255) : new Color(255, 25, 25)));
    }

    /**
     * Try parse the spell
     *
     * @param spell Spell
     * @param caster Caster entity
     * @return True if the spell is valid, otherwise false
     */
    public static Boolean TryParse(String spell, BaseEntity caster) {
        String[] words = spell.toLowerCase().replace(", ", " ").replace(",", "").replace("!", "").split("\\s+");
        for (String word : words) {
            System.out.println(word);
        }
        try {
            switch (words[0]) {
                case "generate":
                    Element etype;
                    switch (words[1]) {
                        case "thermal":
                            etype = THERMAL;
                            break;
                        case "aqueous":
                        case "aqua":
                            etype = AQUA;
                            break;
                        case "aerial":
                        case "air":
                            etype = AERIAL;
                            break;
                        case "cryogenic":
                        case "cryo":
                            etype = CRYO;
                            break;
                        case "luminous":
                            etype = LUMINO;
                            break;
                        case "metallic":
                        case "metal":
                            etype = METAL;
                            break;
                        case "umbra":
                            etype = UMBRA;
                            break;
                        default:
                            etype = NULL;
                            break;
                    }
                    if (etype.toString().equals("NULL")) {
                        putLog("Invalid element!", caster);
                    } else if (caster.mana >= 15) {
                        putLog("Generated " + etype.toString().toLowerCase() + " element!", caster);
                        caster.mana -= 15;
                        caster.currentElement = etype;
                    }

                    break;
                case "throw":
                    if (caster.currentElement != NULL && caster.mana >= 5) {
                        if ("enemy".equals(words[2]) || "player".equals(words[2])) {
                            //log += "Throw element " + e.type.toString().toLowerCase() + " at " + words[2] + ".\n";
                            /*switch (words[2]) {
                             case "enemy":
                             e.target = Game.getInstance().getCurrentEnemy();
                             break;
                             case "player":
                             e.target = EntitySystem.getInstance().getPlayer();
                             break;
                             }*/
                        } else {
                            putLog("Invalid target.", caster);
                        }
                        caster.mana -= 5;
                    } else {
                        System.out.println("Nothing to throw.");
                    }
                    break;
                case "burst":
                    if (caster.currentElement != NULL && caster.mana >= 25) {
                        if (caster.equals(Game.getInstance().player)) {
                            Game.getInstance().getCurrentEnemy().life -= 50;
                        } else {
                            Game.getInstance().player.life -= 40;
                        }
                    } else {
                        putLog("Nothing to burst!", caster);
                        return false;
                    }
                    caster.currentElement = NULL;
                    caster.mana -= 25;
                    putLog("Burst element!", caster);
                    break;
                case "discharge":
                    if (caster.currentElement != NULL && caster.mana >= 10) {
                        if (caster.equals(Game.getInstance().player)) {
                            Game.getInstance().getCurrentEnemy().life -= 25;
                        } else {
                            Game.getInstance().player.life -= 20;
                        }
                    } else {
                        putLog("Nothing to Discharge!", caster);
                        return false;
                    }
                    caster.currentElement = NULL;
                    caster.mana -= 10;
                    putLog("Discharge element!", caster);
                    break;
                case "inspect":
                    String command = "";
                    for (String word : words) {
                        command += word + ' ';
                    }
                    if ("entire command list".contains(command.substring(0, command.length() - 1).replace("inspect", ""))) {
                        putLog("SYSTEM ALERT\nUNAUTHORIZED ACCESS\nSYSTEM ALERT", caster);
                    }
                    break;
                case "summon":
                    switch (words[1]) {
                        case "friend":
                            EntitySystem.getInstance().addEntity(new FriendlyFriend());
                            caster.mana -= 50;
                            break;
                        case "minion":
                            if(!caster.equals(EntitySystem.getInstance().getPlayer()))
                            {
                                EntitySystem.getInstance().addEntity(new EnemyMinion(caster));
                                caster.mana -= 50;
                            } else {
                                putLog("You are not permitted to summon creatures from the Land of Darkness.", caster);
                            }
                    }
                case "counter":
                    //TODO: if the current element can counter the enemy's, then try to block it
                    break;
                case "sudoku":
                    caster.life = 0;
                    putLog("Commited sudoku", caster);
                    break;
            }
        } catch (IndexOutOfBoundsException ex) {
            return false;
        }
        return true;
    }
}
