/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo;

import java.nio.file.Paths;
import org.jsfml.audio.*;

/**
 *
 * @author easimer
 */
public class MusicBox {
    
    Music m;
    
    public MusicBox(String path)
    {
        try {
            m = new Music();
            m.openFromFile(Paths.get(path));
        } catch (Exception ex) {
            
        }
    }
    
    public void playMusic() {
        m.play();
    }
    
    public void pauseMusic() {
        m.pause();
    }
    
    public void loopMusic(Boolean b) {
        m.setLoop(b);
    }
}
