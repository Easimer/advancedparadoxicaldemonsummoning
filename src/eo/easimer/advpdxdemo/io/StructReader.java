package eo.easimer.advpdxdemo.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author easimer
 * @param <T> Type
 */
public class StructReader<T> {

    FileInputStream fs;
    public ObjectInputStream is;

    public StructReader(File f) {
        try {
            if (!f.exists()) {
                throw new IllegalArgumentException("File not found: " + f.getCanonicalPath());
            }
            fs = new FileInputStream(f);
            is = new ObjectInputStream(fs);
        } catch (IOException ioe) {
            System.out.println("IO Error");
        }
    }

    public T readObject() {
        try {
            return (T) is.readObject();
        } catch (IOException ex) {
            System.out.println("IO Error");
        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found");
        }
        return null;
    }

    public void close() {
        try {
            is.close();
            fs.close();
        } catch (IOException ex) {
            System.out.println("IO Error");
        }
    }
}
