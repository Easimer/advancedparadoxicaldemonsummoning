package eo.easimer.advpdxdemo;

import eo.easimer.advpdxdemo.gui.GUIElement;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class XitranGUI {

    private HashMap<Integer, GUIElement> elements;

    private static volatile XitranGUI instance;

    public static synchronized XitranGUI getInstance() {
        if (instance == null) {
            instance = new XitranGUI();
        }
        return instance;
    }

    public static synchronized XitranGUI newInstance() {
        instance = new XitranGUI();
        return instance;
    }

    private XitranGUI() {
        elements = new HashMap<>();
    }

    public void tick(double dt) {
        try {
            for (GUIElement e : elements.values()) {
                e.tick(dt);
            }
        } catch (ConcurrentModificationException ex) {
        }
    }

    public void draw(RenderTarget rt) {
        try {
            for (GUIElement e : elements.values()) {
                e.draw(rt);
            }
        } catch (ConcurrentModificationException ex) {
        }
    }

    public int addEntity(GUIElement ent) {
        int id = -1;
        if (!elements.containsValue(ent)) {
            id = elements.keySet().toArray().length;
            elements.put(id, ent);
            ent.init();
            System.out.format("Added entity with class \"%s\" with ID %d.\n", ent.toString(), id);
        }

        return id;
    }

    public GUIElement getElement(int id) {
        if (elements.containsKey(id)) {
            return elements.get(id);
        } else {
            return null;
        }
    }

    private static ArrayList<Object> getKeysFromValue(HashMap<?, ?> hm, Object value) {
        ArrayList<Object> list = new ArrayList<>();
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                list.add(o);
            }
        }
        return list;
    }

    public int getElementID(GUIElement element) {
        if (elements.containsValue(element)) {
            return (int) getKeysFromValue(elements, element).get(0);

        } else {
            return -1;
        }
    }

    public void removeElement(int key) {
        if (elements.containsKey(key)) {
            elements.remove(key);
        }
    }

    public Boolean event(Event event, Event.Type eventType) {
        for (GUIElement e : elements.values()) {
            e.event(event, eventType);
        }
        return true;
    }
}
