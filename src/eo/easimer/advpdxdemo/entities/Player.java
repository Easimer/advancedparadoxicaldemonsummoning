package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.Game;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsfml.graphics.Color;
import org.jsfml.graphics.Image;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Shader;
import org.jsfml.graphics.ShaderSourceException;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.graphics.TextureCreationException;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class Player extends BaseEntity {

    Boolean chanting = false;
    Texture effectChantingT;
    Sprite effectChantingS;
    Boolean chantingA = true;
    int chantingTrans = 0;

    public int level = 1;
    public int experience = 0;
    Boolean levelUp = false;
    int[] levelUpTable = { //lvl2 req xp, lvl3 req xp, etc.
        500, 750, 1000, 1500, 2000, 3000, 3500, 4000, 4500, 5000, 6000, 7500, 10000, 12500, 15000, 19000, 25000, 30000, 40000, 50000, 65000, 85000, 100000
    };

    Vector2f chantingPos;

    @Override
    public void init() throws IOException {
        texture = new Texture();
        effectChantingT = new Texture();
        Image image = new Image();
        image.loadFromFile(Paths.get("data/wizardsantathing.png"));
        image.createMaskFromColor(Color.MAGENTA);
        try {
            texture.loadFromImage(image);
            effectChantingT.loadFromFile(Paths.get("data/chanting.png"));
        } catch (TextureCreationException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        effectChantingS = new Sprite(effectChantingT);
        effectChantingS.setPosition(80, 600 - effectChantingT.getSize().y);
        chantingPos = effectChantingS.getPosition();
        sprite = new Sprite(texture);

        sprite.setPosition(80, 610 - texture.getSize().y);
        pos = new Vector2i(80, 610 - texture.getSize().y);
        life = 300;
        mana = 250;
        maxLife = 300;
        maxMana = 250;
    }

    @Override
    public void tick(double dt) {
        if (life <= 0 && !dead) {

            try {
                Image i = new Image();
                i.loadFromFile(Paths.get("data/grave.png"));
                i.createMaskFromColor(Color.MAGENTA);
                texture.loadFromImage(i);
                sprite = new Sprite(texture);
                sprite.setPosition(pos.x, pos.y + 40);
            } catch (IOException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            } catch (TextureCreationException ex) {
                Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
            }
            dead = true;

        }
        regeneration(dt);
        levelUp = levelUpTable[level - 1] >= experience;
        if (levelUp) {
            maxLife = 250 * level;
            maxMana = 150 * level;
            levelUp = false;
        }
    }

    @Override
    public void event(Event e, Event.Type et) {
    }

    @Override
    public void draw(RenderTarget rt) {
        rt.draw(sprite);
        if (chanting) {
            rt.draw(effectChantingS);
        }
    }
}
