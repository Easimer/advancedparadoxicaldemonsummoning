package eo.easimer.advpdxdemo.entities;

/**
 *
 * @author easimer
 */
public interface AI {

    public void tick(BaseEntity entity);

    public void init();
}
