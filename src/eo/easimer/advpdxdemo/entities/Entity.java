package eo.easimer.advpdxdemo.entities;

import java.io.IOException;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public interface Entity {

    public void init() throws IOException;

    public void tick(double dt);

    public void draw(RenderTarget rt);

    public void event(Event e, Event.Type et);
}
