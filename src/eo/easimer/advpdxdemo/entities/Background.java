package eo.easimer.advpdxdemo.entities;

import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class Background extends BaseEntity {

    @Override
    public void init() throws IOException {
        texture = new Texture();
        texture.loadFromFile(Paths.get("data/background_snowarena.png"));
        sprite = new Sprite(texture);
        sprite.setScale(2, 2);
        sprite.setPosition(0, 0);
    }
}
