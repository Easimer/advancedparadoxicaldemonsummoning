package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.EntitySystem;
import eo.easimer.advpdxdemo.Game;

/**
 *
 * @author easimer
 */
public class EnemyEntity extends BaseEntity {

    AI ai;
    int level = 1;

    @Override
    public void tick(double dt) {
        ai.tick(this);
        if (life <= 0) {
            onDeath();
        }

    }

    public void onDeath() {
        EntitySystem.getInstance().getPlayer().experience += Math.exp(level) * 10; //on death, give player e^level * 10 experience points
        Game.getInstance().score++;
        dead = true;
        Game.getInstance().destroyCurrentEnemy();

        System.out.println("I died");
    }
}
