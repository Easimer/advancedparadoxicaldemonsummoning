package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.Game.Element;
import static eo.easimer.advpdxdemo.Game.Element.NULL;
import eo.easimer.advpdxdemo.EntitySystem;
import eo.easimer.advpdxdemo.SacredArtParser;
import java.util.Random;

/**
 *
 * @author easimer
 */
public class FriendAI implements AI {

    Random r;
    String[] offensiveElements = {"thermal"};

    public FriendAI() {
        init();
    }

    @Override
    public void init() {
        r = new Random();
    }

    @Override
    public void tick(BaseEntity ent) {
        if (r.nextFloat() < 0.01f && ent.mana > 25) {
            if (ent.currentElement == NULL) {
                SacredArtParser.TryParse("generate " + offensiveElements[r.nextInt(offensiveElements.length)] + " element", ent);
                SacredArtParser.TryParse("Throw at enemy", ent);
            }
            if (ent.currentElement != NULL) {
                if (ent.mana > 10) {
                    if (ent.mana < 25) {
                        SacredArtParser.TryParse("Discharge", ent);
                    } else {
                        SacredArtParser.TryParse("Burst", ent);
                    }

                }
            }
        }
    }

}
