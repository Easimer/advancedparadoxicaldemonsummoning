package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.EntitySystem;
import eo.easimer.advpdxdemo.SacredArtParser;
import eo.easimer.advpdxdemo.Game.Element;
import static eo.easimer.advpdxdemo.Game.Element.NULL;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author easimer
 */
public class EnemyAI implements AI {

    Random r;
    String[] offensiveElements = {"thermal"};
    int delay = 10;
    int cDelay = 0;

    public EnemyAI() {
        init();
    }

    @Override
    public void init() {
        r = new Random();
    }

    @Override
    public void tick(BaseEntity ent) {
        if (cDelay < delay) {
            cDelay++;
        } else {
            if (ent.currentElement != NULL && !EntitySystem.getInstance().getPlayer().dead) {
                if (ent.mana > 10 && r.nextBoolean()) {
                    if (ent.mana < 25 && ent.currentElement != NULL) {
                        SacredArtParser.TryParse("Discharge", ent);
                    } else {
                        SacredArtParser.TryParse("Burst", ent);
                    }

                }
            } else if (ent.mana > 25) {
                if (ent.life < ent.maxLife / 2) {
                    if (r.nextFloat() < 0.1f) {
                        SacredArtParser.TryParse("full life recovery", ent);
                    }
                    SacredArtParser.TryParse("generate luminous element", ent);
                    SacredArtParser.TryParse("discharge!", ent);
                }
                if (ent.currentElement == NULL) {
                    SacredArtParser.TryParse("generate " + offensiveElements[r.nextInt(offensiveElements.length)] + " element", ent);
                }

            }
            cDelay = 0;
        }

    }

}
