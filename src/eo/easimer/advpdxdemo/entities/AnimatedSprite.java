package eo.easimer.advpdxdemo.entities;

import org.jsfml.graphics.IntRect;
import org.jsfml.graphics.RenderTarget;

/**
 *
 * @author easimer
 */
public class AnimatedSprite extends BaseEntity {

    int frame = 0;
    int maxFrame = 0;

    @Override
    public void draw(RenderTarget rt) {
        sprite.setTextureRect(new IntRect(frame * (texture.getSize().x), frame * (texture.getSize().y / maxFrame), texture.getSize().x, texture.getSize().y / maxFrame));
        rt.draw(sprite);
        frame = (frame == maxFrame) ? 0 : frame + 1;
    }
}
