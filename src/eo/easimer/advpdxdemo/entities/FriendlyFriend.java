/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.EntitySystem;
import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;

/**
 *
 * @author easimer
 */
public class FriendlyFriend extends BaseEntity {

    AI ai;

    public FriendlyFriend() {
        ai = new FriendAI();
    }

    @Override
    public void init() throws IOException {
        texture = new Texture();
        texture.loadFromFile(Paths.get("data/friend.png"));
        sprite = new Sprite(texture);
        Vector2f playerPos = EntitySystem.getInstance().getPlayer().sprite.getPosition();
        sprite.setPosition(new Vector2f(playerPos.x + 150, playerPos.y - 150));
    }

    @Override
    public void tick(double dt) {
        ai.tick(this);
        mana = 500;
    }
}
