package eo.easimer.advpdxdemo.entities;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.logging.Logger;
import org.jsfml.graphics.Color;
import org.jsfml.graphics.Image;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.graphics.TextureCreationException;
import org.jsfml.system.Vector2i;

/**
 *
 * @author easimer
 */
public class EnemyWizard extends EnemyEntity {

    @Override
    public void init() throws IOException {
        ai = new EnemyAI();
        texture = new Texture();
        Image image = new Image();
        image.loadFromFile(Paths.get("data/evilwizard.png"));
        image.createMaskFromColor(Color.MAGENTA);
        try {
            texture.loadFromImage(image);
        } catch (TextureCreationException ex) {
            Logger.getLogger(Player.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        sprite = new Sprite(texture);
        sprite.setPosition(1000, 590 - texture.getSize().y + 20);
        pos = new Vector2i(1000, 590 - texture.getSize().y + 20);
        life = 75;
        mana = 100;
        maxLife = 75;
        maxMana = 100;
    }

    @Override
    public void tick(double dt) {
        ai.tick(this);
        //regeneration(dt, true); //this shit ain't working
        mana = maxMana;
        if (life <= 0) {
            onDeath();
        }
    }
}
