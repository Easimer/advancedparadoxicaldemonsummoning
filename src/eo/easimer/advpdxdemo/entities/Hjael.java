package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.EntitySystem;
import eo.easimer.advpdxdemo.SacredArtParser;
import static eo.easimer.advpdxdemo.SacredArtParser.log;
import java.io.IOException;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.window.Keyboard;
import org.jsfml.window.Keyboard.Key;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class Hjael implements Entity {

    public static String command = "";
    Player p;

    public Hjael() {
        p = (Player) EntitySystem.getInstance().getEntity(EntitySystem.playerID);
    }

    @Override
    public void init() throws IOException {

    }

    @Override
    public void tick(double dt) {
        p.chanting = !command.equals("");
        if (log.size() > 3) {
            log.remove(log.entrySet().iterator().next().getValue());
        }
    }

    @Override
    public void draw(RenderTarget rt) {
    }

    @Override
    public void event(Event e, Event.Type et) {
        if (!p.dead) {
            if (et == Event.Type.KEY_PRESSED) {
                Keyboard.Key key = e.asKeyEvent().key;
                if (key == Key.RETURN) {
                    SacredArtParser.TryParse(command, p);
                    command = "";
                    return;
                } else if (key == Key.BACKSPACE) {
                    command = "";
                    return;
                }
            }
            if (et == Event.Type.TEXT_ENTERED) {
                char c = e.asTextEvent().character;
                command += c;
                command = command.replace(Character.toString((char) 13), "").replace(Character.toString((char) 8), "");
            }
        }
    }

}
