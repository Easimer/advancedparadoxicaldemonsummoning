package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.Game.Element;
import static eo.easimer.advpdxdemo.Game.Element.NULL;
import java.io.IOException;
import eo.easimer.advpdxdemo.Position;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2i;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class BaseEntity implements Entity {

    public Vector2i pos;
    public Texture texture;
    public Sprite sprite;

    public Element currentElement = NULL;

    public int mana;
    public int life;
    public int maxMana;
    public int maxLife;

    public Boolean dead = false;

    public BaseEntity() {
        this(0, 0);
    }

    public BaseEntity(int x, int y) {
        pos = new Vector2i(x, y);
        try {
            init();
        } catch (IOException ex) {
            System.out.format("Error: %s", ex.getMessage());
        }
    }

    @Override
    public void init() throws IOException {
    }

    @Override
    public void tick(double dt) {
    }

    @Override
    public void draw(RenderTarget rt) {
        rt.draw(sprite);
    }

    @Override
    public void event(Event e, Event.Type et) {
    }

    public Texture getTexture() {
        return texture;
    }

    public void regeneration(double dt) {
        regeneration(dt, false);
    }

    public void regeneration(double dt, Boolean manaOnly) {
        if (life < maxLife && !manaOnly) {
            life += (life + dt / 1000 > maxLife) ? 0 : dt / 1000;
        }
        if (mana < maxMana) {
            mana += (mana + dt / 1000 > maxMana) ? 0 : dt / 1000;
        }
    }
}
