package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.EntitySystem;
import eo.easimer.advpdxdemo.Game;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsfml.graphics.Color;
import org.jsfml.graphics.Image;
import org.jsfml.graphics.IntRect;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.graphics.TextureCreationException;
import org.jsfml.system.Vector2i;

/**
 *
 * @author easimer
 */
public class EnemySnowman extends EnemyEntity {

    @Override
    public void init() throws IOException {
        ai = new EnemyAI();
        life = 20;
        maxLife = 20;
        mana = 1;
        maxMana = 1;
        Image image = new Image();
        image.loadFromFile(Paths.get("data/snowman.png"));
        image.createMaskFromColor(Color.MAGENTA);
        texture = new Texture();
        try {
            texture.loadFromImage(image);
        } catch (TextureCreationException ex) {
            Logger.getLogger(EnemySnowman.class.getName()).log(Level.SEVERE, null, ex);
        }
        sprite = new Sprite(texture);
        Random r = new Random();
        int n = r.nextInt(5);
        System.out.println("Snowman No. " + n);
        sprite.setTextureRect(new IntRect(0, n * 64, 64, 64));
        sprite.setPosition(1000, 590 - texture.getSize().y / 5);
        pos = new Vector2i(1000, 590 - texture.getSize().y / 5);
    }

    @Override
    public void tick(double dt) {
        ai.tick(this);
        if (life <= 0) {
            onDeath();
        }

    }

    @Override
    public void onDeath() {
        EntitySystem.getInstance().getPlayer().experience += Math.exp(level) * 10; //on death, give player e^level * 10 experience points
        Game.getInstance().score++;
        ai = null;
        level = 0;
        texture = null;
        sprite = null;
        pos = null;
        life = 0;
        mana = 0;
        dead = true;
        Game.getInstance().destroyCurrentEnemy();
        System.out.println("I died");
    }
}
