/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo.entities;

import static eo.easimer.advpdxdemo.Game.Element.NULL;
import eo.easimer.advpdxdemo.SacredArtParser;
import java.util.Random;

/**
 *
 * @author easimer
 */
public class EnemyMinionAI implements AI {
    Random r;
    String[] offensiveElements = {"thermal"};

    public EnemyMinionAI() {
        init();
    }

    @Override
    public void init() {
        r = new Random();
    }

    @Override
    public void tick(BaseEntity ent) {
        if (r.nextFloat() < 0.01f && ent.mana > 25) {
            if (ent.currentElement == NULL) {
                SacredArtParser.TryParse("generate " + offensiveElements[r.nextInt(offensiveElements.length)] + " element", ent);
                SacredArtParser.TryParse("Throw at player", ent);
            }
            if (ent.currentElement != NULL) {
                if (ent.mana > 10) {
                    if (ent.mana < 25) {
                        SacredArtParser.TryParse("Discharge", ent);
                    } else {
                        SacredArtParser.TryParse("Burst", ent);
                    }

                }
            }
        }
    }
}
