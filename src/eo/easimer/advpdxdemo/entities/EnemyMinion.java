package eo.easimer.advpdxdemo.entities;

import eo.easimer.advpdxdemo.EntitySystem;
import java.io.IOException;
import java.nio.file.Paths;
import org.jsfml.graphics.Sprite;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;
import org.jsfml.system.Vector2i;

/**
 *
 * @author easimer
 */
public class EnemyMinion extends EnemyEntity {

    BaseEntity master;

    public EnemyMinion() {
        try {
            init();
        } catch (IOException ex) {

        }
    }

    public EnemyMinion(BaseEntity master) {
        this.master = master;
        try {
            init();
        } catch (IOException ex) {

        }
    }

    @Override
    public void init() throws IOException {
        ai = new EnemyMinionAI();
        texture = new Texture();
        texture.loadFromFile(Paths.get("data/minion.png"));
        sprite = new Sprite(texture);

        if (master != null) {
            Vector2i masterPos = master.pos;
            sprite.setPosition(new Vector2f(masterPos.x + 150, masterPos.y - 150));
        } else {
            sprite.setPosition(1000, 590 - texture.getSize().y);
            pos = new Vector2i(1000, 590 - texture.getSize().y);
            maxLife = 10;
            life = 10;
            maxMana = 100;
            mana = 100;
        }
    }
}
