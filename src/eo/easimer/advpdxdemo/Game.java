package eo.easimer.advpdxdemo;

import eo.easimer.advpdxdemo.entities.Background;
import eo.easimer.advpdxdemo.entities.EnemyEntity;
import eo.easimer.advpdxdemo.entities.EnemyMinion;
import eo.easimer.advpdxdemo.entities.EnemySnowman;
import eo.easimer.advpdxdemo.entities.EnemyWizard;
import eo.easimer.advpdxdemo.entities.Hjael;
import eo.easimer.advpdxdemo.entities.Level;
import eo.easimer.advpdxdemo.entities.Player;
import eo.easimer.advpdxdemo.gui.GUIHUD;
import eo.easimer.advpdxdemo.gui.GUILogo;
import eo.easimer.advpdxdemo.gui.GUIScore;
import eo.easimer.advpdxdemo.gui.GUISpellField;
import eo.easimer.advpdxdemo.gui.GUISpellLog;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Random;
import java.util.logging.Logger;
import org.jsfml.graphics.*;
import org.jsfml.window.*;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class Game {

    RenderWindow window;
    VideoMode vmode;
    public static EntitySystem entitySystem;
    private Boolean _running = true;
    private static Color CornflowerBlue = new Color(100, 149, 237, 255);
    double oldTime = 0.0;
    double newTime = 0.0;
    Level currentLevel;
    Random r;
    public static Font font;
    Player player;
    public int score = 0;
    public static EnemyEntity currentEnemy;
    public static GUIHUD currentHUD;
    
    public static MusicBox bgmusic;
    
    private static volatile Game instance = null;

    public static enum Element {

        THERMAL,
        AQUA,
        AERIAL,
        CRYO,
        LUMINO,
        METAL,
        UMBRA,
        NULL
    }

    public static synchronized Game getInstance() {
        if (instance == null) {
            instance = newInstance();
        }
        return instance;
    }

    public static synchronized Game newInstance() {
        return newInstance(1280, 720, 60, "Advanced Paradoxical Demon Summoning: or, 101 Christmas Cookies");
    }

    public static synchronized Game newInstance(int width, int height, int fpslimit, String title) {
        instance = new Game(width, height, fpslimit, title);
        return instance;
    }

    private Game(int width, int height, int fpslimit, String title) {
        r = new Random();
        window = new RenderWindow();
        vmode = new VideoMode(width, height);
        window.create(vmode, title);
        window.setFramerateLimit(fpslimit);
        font = new Font();
        try {
            font.loadFromFile(Paths.get("data/dos.ttf"));
        } catch (IOException ex) {
            //Failed to load font
            ex.printStackTrace();
        }
        
        bgmusic = new MusicBox("data/music.wav");
        org.jsfml.internal.SFMLNative.
        
        entitySystem = EntitySystem.newInstance();
        //Add background
        EntitySystem.getInstance().addEntity(new Background());
        //Add player
        player = new Player();
        EntitySystem.getInstance().addEntity(player);
        //Add gods (magic spell parser, etc.)
        EntitySystem.getInstance().addEntity(new Hjael());
        //Add Spell Log
        XitranGUI.getInstance().addEntity(new GUISpellLog());
        //Add Spell Field
        XitranGUI.getInstance().addEntity(new GUISpellField());
        //Add HP/MP bars
        XitranGUI.getInstance().addEntity(new GUIHUD(player));
        //Add score
        XitranGUI.getInstance().addEntity(new GUIScore());
        //Add logo
        XitranGUI.getInstance().addEntity(new GUILogo());
        bgmusic.loopMusic(true);
        bgmusic.playMusic();
    }

    public void Loop() {
        while (window.isOpen()) {
            window.clear(CornflowerBlue);
            for (Event event : window.pollEvents()) {
                if (event.type == Event.Type.CLOSED) {
                    window.close();
                } else {
                    EntitySystem.getInstance().event(event, event.type);
                    XitranGUI.getInstance().event(event, event.type);
                }
            }
            //Generate enemy
            if (currentEnemy == null) {
                /*float f = r.nextFloat();
                 if(f > 0.9f)
                 currentEnemy = new EnemyEntity();
                 else */
                int n = r.nextInt(3);
                switch (n) {
                    case 0:
                        currentEnemy = new EnemySnowman();
                        break;
                    case 1:
                        currentEnemy = new EnemyWizard();
                        break;
                    case 2:
                        currentEnemy = new EnemyMinion();
                        break;
                }
                //currentEnemy = new EnemyWizard();
                XitranGUI.getInstance().addEntity(new GUIHUD(currentEnemy));
            }
            oldTime = newTime;
            newTime = System.nanoTime() / 1000000;
            window.setTitle("Advanced Paradoxical Demon Summoning: or, 101 Christmas Cookies - dt: " + (newTime - oldTime));
            EntitySystem.getInstance().tick(newTime - oldTime);
            EntitySystem.getInstance().draw(window);
            getCurrentEnemy().tick(oldTime - newTime);
            getCurrentEnemy().draw(window);
            XitranGUI.getInstance().tick(newTime - oldTime);
            XitranGUI.getInstance().draw(window);
            window.display();
        }
    }

    public void GameOver() {

    }

    public void destroyCurrentEnemy() {
        currentEnemy = new EnemySnowman();
        XitranGUI.getInstance().addEntity(new GUIHUD(currentEnemy));
    }

    public EnemyEntity getCurrentEnemy() {
        if (currentEnemy == null) {
            EnemyEntity e = new EnemySnowman();
            currentEnemy = e;
            XitranGUI.getInstance().addEntity(new GUIHUD(currentEnemy));
            return e;
        } else {
            return currentEnemy;
        }
    }
}
