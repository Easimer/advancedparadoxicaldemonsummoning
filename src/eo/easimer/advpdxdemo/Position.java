package eo.easimer.advpdxdemo;

/**
 *
 * @author easimer
 */
public class Position {

    public int X, Y;

    public Position() {
        this(0, 0);
    }

    public Position(int x, int y) {
        X = x;
        Y = y;
    }

    /**
     * Add p to this position
     *
     * @param p
     */
    public void add(Position p) {
        X = X + p.X;
        Y = Y + p.Y;
    }
}
