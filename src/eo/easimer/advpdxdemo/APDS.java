package eo.easimer.advpdxdemo;

/**
 *
 * @author easimer
 */
public class APDS {

    static Game _g;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        _g = Game.newInstance();
        _g.Loop();
    }

}
