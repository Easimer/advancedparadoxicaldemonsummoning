/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo.gui;

import eo.easimer.advpdxdemo.Game;
import eo.easimer.advpdxdemo.XitranGUI;
import eo.easimer.advpdxdemo.entities.BaseEntity;
import org.jsfml.graphics.Color;
import org.jsfml.graphics.RectangleShape;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class GUIHUD implements GUIElement {
    
    public int id = -1;
    int life = 0;
    int mana = 0;
    BaseEntity entity;
    
    int maxsize = 0;
    
    Text text;

    /**
     * Create new HUD
     *
     * @param entity Entity to assign the hud to
     */
    public GUIHUD(BaseEntity entity) {
        this.entity = entity;
    }
    
    @Override
    public void init() {
        id = XitranGUI.getInstance().getElementID(this);
        text = new Text("", Game.font, 16);
        text.setPosition(entity.pos.x + entity.texture.getSize().x / 2, entity.pos.y - 50);
        tick(0);
    }
    
    @Override
    public void tick(double dt) {
        if (entity != null) {
            text.setString("HP: " + entity.life + "/" + entity.maxLife + "\nMP: " + entity.mana + "/" + entity.maxMana);
            if (entity.dead) {
                System.out.format("HUD#%d: user is dead, deleting myself\n", id);
                XitranGUI.getInstance().removeElement(id);
            }
        }
    }
    
    @Override
    public void draw(RenderTarget rt) {
        rt.draw(text);
    }
    
    @Override
    public void event(Event e, Event.Type et) {
    }
    
}
