/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo.gui;

import eo.easimer.advpdxdemo.Game;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.system.Vector2f;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class GUIScore implements GUIElement {

    Text text;
    int score = -1;

    @Override
    public void init() {
        text = new Text("", Game.font, 16);
        text.setPosition(10, 10);
    }

    @Override
    public void tick(double dt) {
        if (score != Game.getInstance().score) {
            score = Game.getInstance().score;
            text.setString("Score: " + score);
        }
    }

    @Override
    public void draw(RenderTarget rt) {
        rt.draw(text);
    }

    @Override
    public void event(Event e, Event.Type et) {
    }

}
