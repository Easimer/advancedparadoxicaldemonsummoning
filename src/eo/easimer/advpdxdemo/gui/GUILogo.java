/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo.gui;

import eo.easimer.advpdxdemo.Game;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class GUILogo implements GUIElement {

    Text text;

    @Override
    public void init() {
        text = new Text("Advanced Paradoxical Demon Summoning:\n or, 101 Christmas Cookies", Game.font, 36);
        text.setPosition(300, 620);
    }

    @Override
    public void tick(double dt) {

    }

    @Override
    public void draw(RenderTarget rt) {
        rt.draw(text);
    }

    @Override
    public void event(Event e, Event.Type et) {
    }

}
