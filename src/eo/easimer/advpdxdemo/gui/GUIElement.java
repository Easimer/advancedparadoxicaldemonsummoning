package eo.easimer.advpdxdemo.gui;

import eo.easimer.advpdxdemo.Position;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public interface GUIElement {

    public void init();

    public void tick(double dt);

    public void draw(RenderTarget rt);

    public void event(Event e, Event.Type et);
}
