/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eo.easimer.advpdxdemo.gui;

import eo.easimer.advpdxdemo.Game;
import eo.easimer.advpdxdemo.SacredArtParser;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class GUISpellLog implements GUIElement {

    Text text;

    Boolean sysalert = false;

    @Override
    public void init() {
        text = new Text("", Game.font, 16);
    }

    @Override
    public void tick(double dt) {
    }

    @Override
    public void draw(RenderTarget rt) {
        int l = 1;
        for (int i : SacredArtParser.log.keySet()) {
            text.setString(SacredArtParser.log.get(i).x);
            text.setColor(SacredArtParser.log.get(i).y);
            rt.draw(text);
            text.setPosition(640, 25 * l++);
        }
    }

    @Override
    public void event(Event e, Event.Type et) {

    }

}
