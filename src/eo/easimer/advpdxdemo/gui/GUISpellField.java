package eo.easimer.advpdxdemo.gui;

import eo.easimer.advpdxdemo.Game;
import eo.easimer.advpdxdemo.entities.Hjael;
import org.jsfml.graphics.RenderTarget;
import org.jsfml.graphics.Text;
import org.jsfml.window.event.Event;

/**
 *
 * @author easimer
 */
public class GUISpellField implements GUIElement {

    Text text;
    String field = "";

    @Override
    public void init() {
        text = new Text(field, Game.font, 16);
        text.setPosition(10, 100);
    }

    @Override
    public void tick(double dt) {
        if (!Hjael.command.equals(field)) {
            field = Hjael.command;
            text.setString(field);
        }
    }

    @Override
    public void draw(RenderTarget rt) {
        rt.draw(text);
    }

    @Override
    public void event(Event e, Event.Type et) {
    }

}
